# Partage Gitlab-CI

![](./gitlab-ci-cd-logo.png)

Partage de morceaux de `.gitlab-ci.yaml`

## Idée

La documentation Gitlab propose déjà des [modèles de `.gitlab-ci.yaml`](https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/lib/gitlab/ci/templates)
et des [tutoriaux de CI/CD par techno](https://docs.gitlab.com/ee/ci/examples/#cicd-examples).

L'idée de ce projet est de partager et réutiliser des exemples de code de CI/CD pratiques.
Ces fichiers pourraient même être directement inclus dans un fichier de CI grâce à la directive [include:remote](https://docs.gitlab.com/ee/ci/yaml/#includeremote)
